import Vue from 'nativescript-vue';
import Vuex from 'vuex';

//import Authentication from './modules/Authentication';
import Database from './modules/Database';

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

const store = new Vuex.Store({
    modules: {
       //   Authentication,
        Database
    },
    strict: debug,
});

Vue.prototype.$store = store;

module.exports = store;
