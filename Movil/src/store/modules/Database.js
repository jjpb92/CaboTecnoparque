import * as types from "../types";
import * as $firebase from 'nativescript-plugin-firebase'
const state = {
    [types.pathStatesPosts] :{},
    [types.pathStatesCategorias] :{},
    [types.pathStatesMensajes] :{}
};
const getters = {
    [types.gettersPosts] : state=>state[types.pathStatesPosts] ,
    [types.gettersCategorias] : state=>state[types.pathStatesCategorias],
    [types.gettersMensajes] : state=>state[types.pathStatesMensajes]
};
const mutations = {
    [types.pathStatesPosts](state,data){
        state[types.pathStatesPosts] = data
    },
    [types.pathStatesCategorias](state,data) {
        state[types.pathStatesCategorias] = data
    },
    [types.pathStatesMensajes](state,data) {
        state[types.pathStatesMensajes] = data
    }


};

const actions = {
    [types.actionsCargar]({dispatch,commit},payload) {
        return new Promise((resolve, reject) => {

            var onValueEvent = function(result) {
                commit(payload,result.value);
                resolve(result);
            };
            // listen to changes in the /companies path
            $firebase.addValueEventListener(onValueEvent, payload).catch(error=>reject(error))
        });
    },
    [types.actionsActualizar]({dispatch,commit},payload) {
        return new Promise((resolve, reject) => {
            $firebase.update(payload[0], payload[1]).then(()=>resolve());
        });
    },


    [types.actionsQuery]({dispatch,commit},payload) {
        return new Promise((resolve, reject) => {
            var onQueryEvent = function(result) {
            // note that the query returns 1 match at a time
            // in the order specified in the query
            if (!result.error) {
                //  console.log("Event type: " + result.type);
                // console.log("Key: " + result.key);
                // console.log("Value: " + JSON.stringify(result.value));
                commit(payload[0],result.value);
                resolve(result.value);
            }else{
                console.log('sin datos')
                reject('Sin Datos')
            }
        };

        $firebase.query(
            onQueryEvent,
            payload[0],
            {
                singleEvent: true,
                // order by company.country
                orderBy: {
                    type: $firebase.QueryOrderByType.CHILD,
                    value: payload[1] // mandatory when type is 'child'
                },
                range: {
                    type: $firebase.QueryRangeType.EQUAL_TO,
                    value: payload[2]
                },
                limit: {
                    type: $firebase.QueryLimitType.LAST,
                    value: 20
                }
            });
        });
    },

    [types.actionsGuardar]({dispatch,commit},payload) {
        return new Promise((resolve, reject) => {
            let data = payload.data;
            let fire = payload.ruta;
            if(data.Uid === undefined || data.Uid === null){
                $firebase.push(fire, data).then(()=>resolve());
            }else{
                dispatch(types.actionsActualizar,[fire+'/'+data.Uid,data]).then(()=>resolve());
            }
        });
    },
    [types.actionsEliminar]({dispatch,commit},payload) {}
};

export default {
    state,
    getters,
    mutations,
    actions,
};
