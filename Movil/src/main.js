import Vue from 'nativescript-vue';
import lodash from 'lodash'
import App from './App';
import * as firebase from 'nativescript-plugin-firebase';
import Pager from 'nativescript-pager/vue';
import store from './store';
import * as types from './store/types';
import './styles.scss';

// Uncommment the following to see NativeScript-Vue output logs
//Vue.config.silent = false;
Object.defineProperty(Vue.prototype, '$types', { value: types });
Object.defineProperty(Vue.prototype, '$firebase', { value: firebase });
Object.defineProperty(Vue.prototype, '$lodash', { value: lodash });

Vue.registerElement('CardView',() => require('nativescript-cardview').CardView);
Vue.registerElement('RadSideDrawer',() => require('nativescript-ui-sidedrawer').RadSideDrawer);
Vue.registerElement('Gradient', () => require('nativescript-gradient').Gradient);
Vue.registerElement('Carousel', () => require('nativescript-carousel').Carousel);
Vue.registerElement('CarouselItem', () => require('nativescript-carousel').CarouselItem);
Vue.registerElement('FrescoDrawee', () => require('nativescript-fresco').FrescoDrawee);

Vue.component('RadListView', require('./RadListView'));

Vue.use(Pager);
import * as permissions from "nativescript-permissions";


new Vue({
  render: h => h(App),
  store,
    created(){
        permissions.requestPermissions([android.Manifest.permission.CALL_PHONE,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE],
            "App Needs The Following permissions")
            .then(()=>{
                console.log("Permission Granted !");
                // Code for using network state and camera goes here
            })
            .catch(()=>{
                console.log("Permission Denied !");
            });
        this.$firebase.init({
            // Optionally pass in properties for database, authentication and cloud messaging,
            // see their respective docs.
            persist: true,
        }).then(
            function (instance) {
                console.log("firebase.init done");

            },
            function (error) {
                console.log("firebase.init error: " + error);
            }
        );

        },
    mounted(){
        this.$store.dispatch(this.$types.actionsCargar,this.$types.pathStatesCategorias).then(()=>{});
        this.$store.dispatch(this.$types.actionsCargar,this.$types.pathStatesPosts).then(()=>{});
        this.$store.dispatch(this.$types.actionsCargar,this.$types.pathStatesMensajes).then(()=>{});
    }
}).$start();
