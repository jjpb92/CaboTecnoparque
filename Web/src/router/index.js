import Vue from 'vue'
import Router from 'vue-router'
import ListaPost from '@/components/ListaPost'
import inicio from '@/components/iniciar'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'inicio',
      component: inicio
    },
    {
      path: '/lista-post',
      name: 'ListaPost',
      component: ListaPost
    }

  ]
})
