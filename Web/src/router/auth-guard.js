import store from '../store'
import * as types from '../store/types'
export default (to, from, next) => {
  let User = store.getters[types.pathStatesGettersUsuario];

  if (Object.keys(User).length === 0) {
    if (to.name === "iniciar") {
      next()
    } else {
      next('/')
    }
  } else{
    next('/')
  }
}
