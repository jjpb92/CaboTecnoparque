import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/storage'
import 'firebase/database'

var config = {
  apiKey: "AIzaSyDHMxMoH4zuiJCPuutvVLkNStYbjfodYhw",
  authDomain: "cabo-app.firebaseapp.com",
  databaseURL: "https://cabo-app.firebaseio.com",
  projectId: "cabo-app",
  storageBucket: "cabo-app.appspot.com",
  messagingSenderId: "578887078345"
};

firebase.initializeApp(config);

export const FirebaseAuth = firebase.auth();
export const FirebaseDatabase = firebase.database().ref();
export const FirebaseStorage = firebase.storage().ref();
