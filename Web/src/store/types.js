//
export var Error = "Error";
export var Cerrar = "Cerrar";
export var NombreApp = "Cabo App";


//tipo de usuarios
export var Adm = "Administrador";
export var Usu = "Usuario";

//mutations
export var mutationsLimpiarError = "LimpiarError";

//rutas-firebase states mutations
export var pathStatesCategorias = "Categorias";
export var pathStatesPosts = "Posts";

//getters
export const gettersCategorias = "get"+pathStatesCategorias;
export const gettersPosts = "get"+pathStatesPosts;
export const gettersError = "get"+Error;
export var pathStatesGettersUsuario = "Usuarios";

//actions
export var actionsCargar = "actionsCargar";
export var actionsActualizar = "actionsActualizar";
export var actionsQuery = "actionsQuery";
export var actionsGuardar = "actionsGuardar";
export var actionsEliminar = "actionsEliminar";
export var actionsonAuthStateChanged = "actionsonAuthStateChanged";
export var actionsGetUser = "actionsGetUser";
export var actionsIngresarEmail = "actionsIngresarEmail";
export var actionsRegistrarseCode = "actionsRegistrarseCode";
export var actionsRegistrarseEmail = "actionsRegistrarseEmail";
export var actionsFacebook = "actionsFacebook";
export var actionsGoogle = "actionsGoogle";
export var actionsRecuperar = "actionsRecuperar";
export var actionsSalir = "actionsSalir";
export var actionsError = "actionsError";
export var actionsSubir = "actionsSubir";
export var actionsDescargar = "actionsDescargar";
export var actionsComprimirImagen = "actionsComprimirImagen";
export var actionsBorrar = "actionsBorrar";


// error
export var errorCorreoDuplicado = "Creating a user failed. com.google.firebase.auth.FirebaseAuthUserCollisionException: The email address is already in use by another account."
export var errorusuarioNull = "Logging in the user failed. com.google.firebase.auth.FirebaseAuthInvalidUserException: There is no user record corresponding to this identifier. The user may have been deleted."
export var errorContrasenaErrada = "Logging in the user failed. com.google.firebase.auth.FirebaseAuthInvalidCredentialsException: The password is invalid or the user does not have a password."
export var errorRequiereCorreo = "Resetting a password requires an email argument"
export var errorIngresoNull = "Auth type PASSWORD requires an \'passwordOptions.email\' and \'passwordOptions.password\' argument"
export var errorRegistroNull = "Creating a user requires an email and password argument"
export var errorRed = "Logging in the user failed. com.google.firebase.FirebaseNetworkException: A network error (such as timeout, interrupted connection or unreachable host) has occurred."

//mensajes
export var mensajeCorreoDuplicado = "La dirección de correo electrónico ya está siendo utilizada por otra cuenta.";
export var mensajeUsuarioNull = "La dirección de correo electrónico no existe.";
export var mensajeEnvioCorreo = "Se a enviado un correo.";
export var mensajeContrasenaErrada = "la contraseña no es válida o el usuario no tiene una contraseña.";
export var mensajeRequiereCorreo = "Restablecer una contraseña requiere un correo electrónico.";
export var mensajeIngresoNull = "Se requiere Correo y Contraseña.";
export var mensajeRed = "Revisa tu conexion a internet.";
export var mensajeSinDatos = "No se encontraron datos";
