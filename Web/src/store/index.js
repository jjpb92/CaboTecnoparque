import Vue from 'vue';
import Vuex from 'vuex';

import Storage from './modules/Storage';
import Database from './modules/Database';
import Authentication from './modules/Authentication';

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

const store = new Vuex.Store({
  modules: {
      Storage,
    Authentication,
      Database
  },
  strict: debug,
});

Vue.prototype.$store = store;

//module.exports = ;
export default store
