import * as types from "../types";
import { FirebaseDatabase } from '../firebase'
const state = {
    [types.pathStatesPosts] :{},
    [types.pathStatesCategorias] :{}
};
const getters = {
    [types.gettersPosts] : state=>state[types.pathStatesPosts] ,
    [types.gettersCategorias] : state=>state[types.pathStatesCategorias]
};
const mutations = {
    [types.pathStatesPosts](state,data){
        state[types.pathStatesPosts] = data
    },
  [types.pathStatesCategorias](state,data){
    state[types.pathStatesCategorias] = data
  }
};

const actions = {
    [types.actionsCargar]({dispatch,commit},payload) {
        return new Promise((resolve, reject) => {
          const fire = FirebaseDatabase.child(payload);
          fire.on('value',(snap)=>{
            if(snap.val()){
              commit(payload,snap.val());
              resolve(snap.val());
            }else{
              reject(types.mensajeSinDatos)
            }
          });
        });
    },

    [types.actionsQuery]({dispatch,commit},payload) {
      return new Promise((resolve, reject) => {
          const fire = FirebaseDatabase.child(payload[0]).orderByChild(payload[1]).equalTo(payload[2]);
          fire.on('value',(snap)=>{
            if(snap.val()){
              commit(payload[0],snap.val());
              resolve(snap.val());
            }else{
              commit(payload[0],{});
              reject(types.mensajeSinDatos)
            }
        });
      });
    },

  [types.actionsActualizar]({dispatch,commit},payload) {
    return new Promise((resolve, reject) => {
      FirebaseDatabase.update(payload)
        .then(function(){resolve()})
        .catch(function(error){reject(error)});
    });
  },

    [types.actionsGuardar]({dispatch,commit},payload) {
        return new Promise((resolve, reject) => {
          //asigno datos a variables locales
          const clone = Object.assign({}, payload);
          let fire = clone[0];
          const data = clone[1];
          //evaluo si es actualizacion o nuevo
          data.Uid = (data.Uid === undefined || data.Uid === null) ? FirebaseDatabase.child(fire).push().key : data.Uid;
          //creacion de la rama de guardado
          var updates = {};

          updates['/'+fire+'/'+data.Uid] = data;



            //Actualizo la informacion
            dispatch(types.actionsActualizar,updates)
              .then(()=>resolve(data))
              .catch((error)=>reject(error))


        });
    },
    [types.actionsEliminar]({dispatch,commit},payload) {
      return new Promise((resolve, reject) => {
        //asigno datos a variables locales
        const Uid = payload[1];
        let fire = payload[0];
        FirebaseDatabase.child(fire).child(Uid).remove()
          .then(function(){resolve()})
          .catch(function(error){reject(error)});
      });

    }
};

export default {
  state,
  getters,
  mutations,
  actions,
};
