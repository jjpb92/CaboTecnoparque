import * as $firebase from '../firebase'
import * as types from '../types'

const state = {
  [types.pathStatesGettersUsuario] :{} ,

};
const getters = {
  [types.pathStatesGettersUsuario] : state=>state[types.pathStatesGettersUsuario] ,
};

const mutations = {
  [types.pathStatesGettersUsuario](state,data){
    state[types.pathStatesGettersUsuario] = data
  }
};

const actions = {

  [types.actionsonAuthStateChanged]({dispatch,commit},payload) {
    return new Promise((resolve, reject) => {
      $firebase.FirebaseAuth.onAuthStateChanged(function(user) {
        if (user) {
          // User is signed in.
          commit(types.pathStatesGettersUsuario,{email:user.email})
          resolve(user)
        } else {
          // No user is signed in.
          reject(null)
        }
      });
    });
  },
  [types.actionsSalir]({dispatch,commit},payload){
    return new Promise((resolve, reject) => {
      $firebase.FirebaseAuth.signOut().then(function() {
        // Sign-out successful.
        commit(types.pathStatesGettersUsuario,{});
        resolve()
      }).catch(function(error) {
        // An error happened.
        reject(error);
      });
    });
  },
  [types.actionsIngresarEmail]({dispatch,commit},payload){
    return new Promise((resolve, reject) => {

      $firebase.FirebaseAuth.signInWithEmailAndPassword(payload.email, payload.password).then(()=>{
        resolve()
      }).catch(function(error) {
        reject(error)
      });

    })
  },
  /*  [types.actionsRegistrarseEmail]({getters,dispatch,commit},payload){
        return new Promise((resolve, reject) => {

            commit(types.Cargando, true);
            commit(types.mutationsLimpiarError);

            $firebase.createUser({
                email: payload.email,
                password: payload.password
            }).then(
                function (result) {
                    const user = Object.assign({}, getters[types.pathStatesPerfil]);
                    user.Uid = result.key;
                    user.Correo = payload.email;
                    $firebase.update(types.pathStatesPerfil + '/' + user.Uid, user);
                    commit(types.Cargando, false);
                    resolve()
                },
                function (errorMessage) {
                    commit(types.Cargando, false);
                    dispatch(types.actionsError,errorMessage).then(()=>reject());

                }
            );
        })

    },

    [types.actionsRecuperar]({dispatch,commit},payload){
        return new Promise((resolve, reject) => {

            commit(types.Cargando, true);
            commit(types.mutationsLimpiarError);
            $firebase.resetPassword({
                email: payload
            }).then(
                function () {
                    commit(types.Cargando, false);
                    commit(types.Error, types.mensajeEnvioCorreo);
                    resolve()
                },
                function (errorMessage) {
                    commit(types.Cargando, false);
                    dispatch(types.actionsError,errorMessage).then(()=>reject());
                }
            );
        })
    },

    [types.actionsError]({dispatch,commit},payload){
        return new Promise((resolve, reject) => {
            console.log(payload);
            switch (payload) {
                case types.errorRequiereCorreo:
                    commit(types.Error, types.mensajeRequiereCorreo);
                    break;
                case types.errorusuarioNull:
                    commit(types.Error, types.mensajeUsuarioNull);
                    break;
                case types.errorContrasenaErrada:
                    commit(types.Error, types.mensajeContrasenaErrada);
                    break;
                case types.errorCorreoDuplicado:
                    commit(types.Error, types.mensajeCorreoDuplicado);
                    break;
                case types.errorIngresoNull:
                    commit(types.Error, types.mensajeIngresoNull);
                    break;
                case types.errorRegistroNull:
                    commit(types.Error, types.mensajeIngresoNull);
                    break;
                case types.errorRed:
                    commit(types.Error, types.mensajeRed);
                    break;
                default:
                    commit(types.Error, payload);
                    break;
            }
            resolve();
        });
    },*/
};

export default {
  state,
  mutations,
  getters,
  actions,
};
