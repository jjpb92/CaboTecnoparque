import * as types from "../types";
import { FirebaseStorage } from '../firebase'

const actions={
  [types.actionsComprimirImagen]({dispatch,commit},payload) {
    return new Promise((resolve, reject) => {
      const filename = payload.name;

      if(filename.lastIndexOf('.')<=0){
        reject('Error Formato')
      }

      const filereader = new FileReader();

      filereader.addEventListener('load',()=>{
        resolve(filereader.result)
      });

      filereader.readAsDataURL(payload);
    });
  },
  [types.actionsSubir]({dispatch,commit},payload) {
    return new Promise((resolve, reject) => {
      let pathfire = payload[0], campo =Object.keys(payload[1])[0], Uid = payload[1][campo], files = payload[2], multiple = payload[3];
      Array.from(Array(files.length).keys()).map( x => {
        const path = (multiple)?'/'+Uid+'/':'/';
        const file = files[x];
          // Upload file and metadata to the object 'images/mountains.jpg'
          FirebaseStorage.child(pathfire+path+file.name).putString(file.src, 'data_url')
            .then((snap)=>{
              snap.ref.getDownloadURL().then(function(downloadURL) {
                //console.log('File available at', downloadURL);
                file.src = downloadURL;
                const path = (multiple)?'/'+file.name:'/';
                var update = {};
                update[pathfire+'/'+Uid+'/'+campo+path] = file;
                console.log(update)
                dispatch(types.actionsActualizar,update)
                  .then(()=>resolve())
                  .catch((error)=>reject(error))
              });
          });
      })
/*
      // Upload file and metadata to the object 'images/mountains.jpg'
      var uploadTask = FirebaseStorage.child(pathfire).putString(file, 'data_url');

// Listen for state changes, errors, and completion of the upload.
      uploadTask.on('state_changed', // or 'state_changed'
        function(snapshot) {
          // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
          var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
          console.log('Upload is ' + progress + '% done');
          switch (snapshot.state) {
            case 'paused': // or 'paused'
              console.log('Upload is paused');
              break;
            case 'running': // or 'running'
              console.log('Upload is running');
              break;
          }
        }, function(error) {
          // A full list of error codes is available at
          // https://firebase.google.com/docs/storage/web/handle-errors
          switch (error.code) {
            case 'storage/unauthorized':
              // User doesn't have permission to access the object
              reject('El usuario no tiene permiso para acceder al objeto');
              break;

            case 'storage/canceled':
              // User canceled the upload
              reject('El usuario canceló la carga');
              break;

            case 'storage/unknown':
              // Unknown error occurred, inspect error.serverResponse
              reject('Se produjo un error desconocido, inspeccionar error.serverResponse');
              break;
          }
        }, function() {
          // Upload completed successfully, now we can get the download URL
          uploadTask.snapshot.ref.getDownloadURL().then((downloadURL) =>{

            if(actualizar){
              update[rutadb].Imagen = downloadURL;
              dispatch(types.actionsActualizar,update)
                .then(()=>resolve())
                .catch((error)=>reject(error))
              }else{
              dispatch(types.actionsGuardar,[rutadb,{file:downloadURL,Uid:name}]).then(()=>resolve(file))
            }
            });

        });*/

    });
  },
  [types.actionsDescargar]({dispatch,commit},payload) {
    return new Promise((resolve, reject) => {

    });
  },
  [types.actionsBorrar]({dispatch,commit},payload) {
    return new Promise((resolve, reject) => {

    });
  }
}
export default {
  actions,
};
