// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import Vuetify from 'vuetify'
import lodash from 'lodash'
import 'vuetify/dist/vuetify.min.css'
import * as types from './store/types'

Vue.use(Vuetify);

Object.defineProperty(Vue.prototype, '$types', { value: types });
Object.defineProperty(Vue.prototype, '$lodash', { value: lodash });


Vue.config.productionTip = false;
window.bus = new Vue();
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>',
  mounted() {
    this.$store.dispatch(this.$types.actionsonAuthStateChanged).then(() => {
      this.$router.replace('lista-post');
    }).catch((error) => {
      this.$router.replace('/')
    })
  }
});
